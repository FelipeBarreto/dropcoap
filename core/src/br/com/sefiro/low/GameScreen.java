package br.com.sefiro.low;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;

import br.com.sefiro.low.models.RainDrop;

public class GameScreen implements Screen {
    final Drop game;

    Texture dropImage;
    Texture bucketImage;
    Sound dropSound;
    Music rainMusic;
    OrthographicCamera camera;
    Rectangle bucket;
    Array<RainDrop> raindrops;
    Pool<RainDrop> rainDropPool;
    long lastDropTime;
    int dropsGathered;

    boolean useTag = false;

    public GameScreen(final Drop game) {
        this.game = game;

        // load the images for the droplet and the bucket, 64x64 pixels each
        dropImage = new Texture(Gdx.files.internal("droplet.png"));
        bucketImage = new Texture(Gdx.files.internal("bucket.png"));

        // load the drop sound effect and the rain background "music"
        dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"));
        rainMusic = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"));
        rainMusic.setLooping(true);

        // create the camera and the SpriteBatch
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        // create a Rectangle to logically represent the bucket
        bucket = new Rectangle();
        bucket.x = 800 / 2 - 64 / 2; // center the bucket horizontally
        bucket.y = 20; // bottom left corner of the bucket is 20 pixels above
        // the bottom screen edge
        bucket.width = 64;
        bucket.height = 64;

        // create the raindrops array and spawn the first raindrop
        raindrops = new Array<RainDrop>();
        rainDropPool = new Pool<RainDrop>() {
            @Override
            protected RainDrop newObject() {
                return new RainDrop();
            }
        };
        spawnRaindrop();

    }

    private void spawnRaindrop() {
        RainDrop raindrop = rainDropPool.obtain();
        raindrop.hitbox.x = MathUtils.random(0, 800 - 64);
        raindrop.hitbox.y = 480;
        raindrop.hitbox.width = 64;
        raindrop.hitbox.height = 64;
        raindrops.add(raindrop);
        lastDropTime = TimeUtils.nanoTime();
    }

    @Override
    public void render(float delta) {
        // clear the screen with a dark blue color. The
        // arguments to glClearColor are the red, green
        // blue and alpha component in the range [0,1]
        // of the color to be used to clear the screen.
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // tell the camera to update its matrices.
        camera.update();

        // tell the SpriteBatch to render in the
        // coordinate system specified by the camera.
        game.batch.setProjectionMatrix(camera.combined);

        // begin a new batch and draw the bucket and
        // all drops
        game.batch.begin();
        game.font.draw(game.batch, "Drops Collected: " + dropsGathered, 0, 480);
        game.batch.draw(bucketImage, bucket.x, bucket.y, bucket.width, bucket.height);
        for (RainDrop raindrop : raindrops) {
            game.batch.draw(dropImage, raindrop.hitbox.x, raindrop.hitbox.y);
        }
        game.batch.end();

        // process user input
        if (Gdx.input.isKeyPressed(Keys.LEFT))
            bucket.x -= 600 * Gdx.graphics.getDeltaTime() * Math.min(3, 1 + (dropsGathered/5)*0.2);;
        if (Gdx.input.isKeyPressed(Keys.RIGHT))
            bucket.x += 600 * Gdx.graphics.getDeltaTime() * Math.min(3, 1 + (dropsGathered/5)*0.2);;

        // process accelerometer
        if(Gdx.app.getType() == Application.ApplicationType.Android){
            double factor = getAccelFactor();
            bucket.x += factor * 200 * Gdx.graphics.getDeltaTime();
        }

        // make sure the bucket stays within the screen bounds
        if (bucket.x < 0)
            bucket.x = 0;
        if (bucket.x > 800 - 64)
            bucket.x = 800 - 64;

        // check if we need to create a new raindrop
        if (TimeUtils.nanoTime() - lastDropTime > 1000000000 * Math.max(0.3334, 1 - (dropsGathered/5)*0.2))
            spawnRaindrop();

        // move the raindrops, remove any that are beneath the bottom edge of
        // the screen or that hit the bucket. In the later case we increase the
        // value our drops counter and add a sound effect.
        Iterator<RainDrop> iter = raindrops.iterator();
        while (iter.hasNext()) {
            RainDrop raindrop = iter.next();
            raindrop.hitbox.y -= 200 * Gdx.graphics.getDeltaTime() * Math.min(3, 1 + (dropsGathered/5)*0.2);
            if (raindrop.hitbox.y + 64 < 0){
                iter.remove();
                rainDropPool.free(raindrop);
            }
            else if (bucket.overlaps(raindrop.hitbox)) {
                dropsGathered++;
                dropSound.play();
                iter.remove();
                rainDropPool.free(raindrop);
            }
        }
    }

    private double getAccelFactor() {
        double y = 0;
        if(useTag){
            if(y > 0){
                y = game.tag.getY() - 2;
            }
            else{
                y = game.tag.getY() + 2;
            }

        }
        else{
            y = Gdx.input.getAccelerometerY();
        }

        if(Math.abs(y) > 1){
            return y * Math.min(3, 1 + (dropsGathered/5)*0.2);
        }
        else{
            return 0;
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
        // start the playback of the background music
        // when the screen is shown
        rainMusic.play();
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        dropImage.dispose();
        bucketImage.dispose();
        dropSound.dispose();
        rainMusic.dispose();
    }

}