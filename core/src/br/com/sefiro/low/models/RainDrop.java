package br.com.sefiro.low.models;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Pool;

public class RainDrop implements Pool.Poolable{

    public Rectangle hitbox;

    public RainDrop(){
        hitbox = new Rectangle();
    }

    @Override
    public void reset() {
        hitbox.x = 0;
        hitbox.y = 0;
    }
}
