package br.com.sefiro.low.interfaces;


public interface ISensorTag {

    void start();
    void stop();
    double getX();
    double getY();
    double getZ();
}
