package br.com.sefiro.low;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import br.com.sefiro.low.interfaces.ISensorTag;


public class Drop extends Game {

    public ISensorTag tag;
    public SpriteBatch batch;
    public BitmapFont font;

    public Drop(ISensorTag tag){
        this.tag = tag;
    }

    public void create() {
        if(tag != null){
            tag.start();
        }

        batch = new SpriteBatch();
        //Use LibGDX's default Arial font.
        font = new BitmapFont();
        this.setScreen(new MainMenuScreen(this));
    }

    public void render() {
        super.render(); //important!
    }

    public void dispose() {
        batch.dispose();
        font.dispose();
        if(tag != null){
            tag.stop();
        }

    }

}