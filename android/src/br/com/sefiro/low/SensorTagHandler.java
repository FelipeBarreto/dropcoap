package br.com.sefiro.low;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.sefiro.low.interfaces.ISensorTag;
import br.ufc.great.sensortag.SensorTag;
import br.ufc.great.sensortag.callbacks.SensorTagListener;

public class SensorTagHandler implements ISensorTag{
    private final Context context;
    private SensorTag tag;

    private double x;
    private double y;
    private double z;

    public SensorTagHandler(Context context){
        this.context = context;
    }

    @Override
    public void start(){
        tag = new SensorTag();
        tag.setPeriod(100);
        tag.start(context, new SensorTagListener() {
            @Override
            public void onSensorTagUpdate(String s) {
                try {
                    JSONObject object = new JSONObject(s);
                    JSONObject accel = object.getJSONObject("AccelData");
                    x = accel.getDouble("X");
                    y = accel.getDouble("Y");
                    z = accel.getDouble("Z");
                    System.out.println(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void stop(){
        if(tag != null){
            tag.stop();
        }
    }

    @Override
    public double getX(){
        return x;
    }

    @Override
    public double getY(){
        return y;
    }

    @Override
    public double getZ(){
        return z;
    }
}
