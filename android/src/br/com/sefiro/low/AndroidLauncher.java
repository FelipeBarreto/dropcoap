package br.com.sefiro.low;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import br.com.sefiro.low.interfaces.ISensorTag;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		ISensorTag tagHandler = new SensorTagHandler(this);
		initialize(new Drop(tagHandler), config);
	}
}
